/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializelab;

import java.io.Serializable;

/**
 *
 * @author Black Dragon
 */
public class Player  implements Serializable{
    private char symbol;
    private int win,lose,draw;

    public Player(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    public int getWin() {
        return win;
    }

    public void win() {
        this.win++;
    }

    public int getLose() {
        return lose;
    }

    public void lose() {
        this.lose++;
    }

    public int getDraw() {
        return draw;
    }

    public void draw() {
        this.draw++;
    }

    @Override
    public String toString() {
        return "Player" + symbol + " win:" + win + ", lose:" + lose + ", draw:" + draw;
    }
}
