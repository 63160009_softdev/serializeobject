/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializelab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Black Dragon
 */
public class WriteFriendList {

    public static void main(String[] args) {
        ArrayList<Friend> friendList = new ArrayList<>();
        
        friendList.add(new Friend("Phadol", 20, "0982500000"));
        friendList.add(new Friend("Erk", 20, "0845953555"));
        friendList.add(new Friend("Boss", 20, "0982555555"));
        
        FileOutputStream fos = null;
        try {
            File file = new File("list_of_friend.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            
            oos.writeObject(friendList);
            
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
